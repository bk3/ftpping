#!/usr/bin/python

import os,sys,logging
from StringIO import StringIO
from time import localtime,strftime
import time
import ftplib

#TODO: outboard S/U/P tuple.

indexTime = "[" + strftime("%m/%d/%Y %H:%M:%S %p %Z",localtime()) + "]"
print "time, server, user, down, error"
try:
  SERVER = ''
  USER   = ''
  PASS   = ''    
  ftp = ftplib.FTP(SERVER,timeout=1)
  ftp.login(USER,PASS)
  ftp.pwd()
  ftp.set_pasv(False)
  r = StringIO()
  ftp.retrbinary('RETR /README', r.write)
  r.truncate(0)
  r.truncate(0)
  ftp.set_pasv(True)
  r = StringIO()
  ftp.retrbinary('RETR /README', r.write)
  r.truncate(0)  
  ftp.quit()
  print "%s, %s, %s, 0, 0" % (indexTime, SERVER, USER)
except (ftplib.error_temp,ftplib.error_reply,ftplib.error_perm,ftplib.error_proto) as e:
  print "%s, %s, %s, 1, %s" % (indexTime, SERVER, USER, e.args[0] )
except (socket.error) as e:
  print "%s, %s, %s, 1, %s" % (indexTime, SERVER, USER, e )
except (IOError) as e:
  print "%s, %s, %s, 1, %s" % (indexTime, SERVER, USER, e.strerror )
finally:
  pass