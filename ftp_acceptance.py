#!/usr/bin/python

import os,sys,logging
from io import BytesIO
import socket
import hashlib
from time import localtime,strftime
import time
import ftplib_indeterminant


"""   
   DATA TRANSFER

   Files are transferred only via the data connection.  The control
   connection is used for the transfer of commands, which describe the
   functions to be performed, and the replies to these commands.
   Several commands are concerned with the transfer of data between hosts.  
   These data transfer commands include the MODE command which specify how
   the bits of the data are to be transmitted, and the STRUcture and TYPE 
   commands, which are used to define the way in which the data are to be 
   represented.  The transmission and representation are basically 
   independent but the "Stream" transmission mode is dependent on the file 
   structure attribute and if "Compressed" transmission mode is used, the 
   nature of the filler byte depends on the representation type.

      DATA REPRESENTATION AND STORAGE

      i) NVT-ASCII Transform
      When data is transferred from a storage device in the sending host 
      to a storage device in the receiving host, the sending and receiving 
      sites perform transformations between VT-ASCII standard representation 
      and their internal representations.

      ii) Word length Transform
      When transmitting binary data between host systems with
      different word lengths,  the user has the option of specifying data
      representation and transformation functions.  

      iii) CRLF Transform

      iv) Padding Transform

      v)  Compression Transform

         DATA TYPES

         Data representation is handled in FTP by a user specifying a
         representation type.  This type may implicitly (as in ASCII or
         EBCDIC) or explicitly (as in Local byte) define a byte size for
         interpretation which is referred to as the "logical byte size."

         Note that this has nothing to do with the byte size used for
         transmission over the data connection, called the "transfer
         byte size", and the two should not be confused.  For example,
         NVT-ASCII has a logical byte size of 8 bits.  If the type is
         Local byte, then the TYPE command has an obligatory second
         parameter specifying the logical byte size.  The transfer byte
         size is always 8 bits.

            ASCII TYPE

            This is the default type and must be accepted by all FTP
            implementations.  It is intended primarily for the transfer
            of text files, except when both hosts would find the EBCDIC
            type more convenient.

            The sender converts the data from an internal character
            representation to the standard 8-bit NVT-ASCII
            representation.  The receiver
            will convert the data from the standard form to his own
            internal form.

            In accordance with the NVT standard, the <CRLF> sequence
            should be used where necessary to denote the end of a line
            of text.

            Using the standard NVT-ASCII representation means that data
            must be interpreted as 8-bit bytes.

            The Format parameter for ASCII and EBCDIC types is discussed
            below.

            IMAGE TYPE

            The data are sent as contiguous bits which, for transfer,
            are packed into the 8-bit transfer bytes.  The receiving
            site must store the data as contiguous bits.  The structure
            of the storage system might necessitate the padding of the
            file (or of each record, for a record-structured file) to
            some convenient boundary (byte, word or block).  This
            padding, which must be all zeros, may occur only at the end
            of the file (or at the end of each record) and there must be
            a way of identifying the padding bits so that they may be
            stripped off if the file is retrieved.  The padding
            transformation should be well publicized to enable a user to
            process a file at the storage site.

            Image type is intended for the efficient storage and
            retrieval of files and for the transfer of binary data.  It
            is recommended that this type be accepted by all FTP
            implementations.

            LOCAL TYPE

            The data is transferred in logical bytes of the size
            specified by the obligatory second parameter, Byte size.
            The value of Byte size must be a decimal integer; there is
            no default value.  The logical byte size is not necessarily
            the same as the transfer byte size.  If there is a
            difference in byte sizes, then the logical bytes should be
            packed contiguously, disregarding transfer byte boundaries
            and with any necessary padding at the end.

            When the data reaches the receiving host, it will be
            transformed in a manner dependent on the logical byte size
            and the particular host.  This transformation must be
            invertible (i.e., an identical file can be retrieved if the
            same parameters are used) and should be well publicized by
            the FTP implementors.

            For example, a user sending 36-bit floating-point numbers to
            a host with a 32-bit word could send that data as Local byte
            with a logical byte size of 36.  The receiving host would
            then be expected to store the logical bytes so that they
            could be easily manipulated; in this example putting the
            36-bit logical bytes into 64-bit double words should
            suffice.

            In another example, a pair of hosts with a 36-bit word size
            may send data to one another in words by using TYPE L 36.
            The data would be sent in the 8-bit transmission bytes
            packed so that 9 transmission bytes carried two host words.

            FORMAT CONTROL

            ASCII and EBCDIC types take a second (optional) parameter; 
            this is to indicate what kind of vertical format control, 
            if any, is associated with a file.  The following data 
            representation types are defined in FTP:

            A character file may be transferred to a host for one of
            three purposes: for printing, for storage and later
            retrieval, or for processing.  If a file is sent for
            printing, the receiving host must know how the vertical
            format control is represented.  In the second case, it must
            be possible to store a file at a host and then retrieve it
            later in exactly the same form.  Finally, it should be
            possible to move a file from one host to another and process
            the file at the second host without undue trouble.  A single
            ASCII or EBCDIC format does not satisfy all these
            conditions.  Therefore, these types have a second parameter
            specifying one of the following three formats:

               NON PRINT FORMAT CONTROL

               This is the default format to be used if the second
               (format) parameter is omitted.  Non-print format must be
               accepted by all FTP implementations.

               The file need contain no vertical format information.  If
               it is passed to a printer process, this process may
               assume standard values for spacing and margins.

               Normally, this format will be used with files destined
               for processing or just storage.

               TELNET FORMAT CONTROL

               The file contains ASCII/EBCDIC vertical format controls
               (i.e., <CR>, <LF>, <NL>, <VT>, <FF>) which the printer
               process will interpret appropriately.  <CRLF>, in exactly
               this sequence, also denotes end-of-line.

               CARRIAGE CONTROL (ASA)

               The file contains ASA (FORTRAN) vertical format control
               characters.  In a line or a record formatted according to
               the ASA Standard, the first character is not to be 
               printed.  Instead, it should be used to determine the 
               vertical movement of the paper which should take place 
               before the rest of the record is printed.

               The ASA Standard specifies the following control
               characters:

                  Character     Vertical Spacing

                  blank         Move paper up one line
                  0             Move paper up two lines
                  1             Move paper to top of next page
                  +             No movement, i.e., overprint

               Clearly there must be some way for a printer process to
               distinguish the end of the structural entity.  If a file
               has record structure (see below) this is no problem;
               records will be explicitly marked during transfer and
               storage.  If the file has no record structure, the <CRLF>
               end-of-line sequence is used to separate printing lines,
               but these format effectors are overridden by the ASA
               controls.

         DATA STRUCTURES

         In addition to different representation types, FTP allows the
         structure of a file to be specified.  Three file structures are
         defined in FTP:

            file-structure,     where there is no internal structure and
                                the file is considered to be a
                                continuous sequence of data bytes,

            record-structure,   where the file is made up of sequential
                                records,

            and page-structure, where the file is made up of independent
                                indexed pages.

         File-structure is the default to be assumed if the STRUcture
         command has not been used but both file and record structures
         must be accepted for "text" files (i.e., files with TYPE ASCII
         or EBCDIC) by all FTP implementations.  The structure of a file
         will affect both the transfer mode of a file (see the Section
         on Transmission Modes) and the interpretation and storage of
         the file.

         The "natural" structure of a file will depend on which host
         stores the file.  A source-code file will usually be stored on
         an IBM Mainframe in fixed length records but on a DEC TOPS-20
         as a stream of characters partitioned into lines, for example
         by <CRLF>.  If the transfer of files between such disparate
         sites is to be useful, there must be some way for one site to
         recognize the other's assumptions about the file.

         With some sites being naturally file-oriented and others
         naturally record-oriented there may be problems if a file with
         one structure is sent to a host oriented to the other.  If a
         text file is sent with record-structure to a host which is file
         oriented, then that host should apply an internal
         transformation to the file based on the record structure.
         Obviously, this transformation should be useful, but it must
         also be invertible so that an identical file may be retrieved
         using record structure.

         In the case of a file being sent with file-structure to a
         record-oriented host, there exists the question of what
         criteria the host should use to divide the file into records
         which can be processed locally.  If this division is necessary,
         the FTP implementation should use the end-of-line sequence,

         <CRLF> for ASCII, or <NL> for EBCDIC text files, as the
         delimiter.  If an FTP implementation adopts this technique, it
         must be prepared to reverse the transformation if the file is
         retrieved with file-structure.

            FILE STRUCTURE

            File structure is the default to be assumed if the STRUcture
            command has not been used.

            In file-structure there is no internal structure and the
            file is considered to be a continuous sequence of data
            bytes.

            RECORD STRUCTURE

            Record structures must be accepted for "text" files (i.e.,
            files with TYPE ASCII or EBCDIC) by all FTP implementations.

            In record-structure the file is made up of sequential
            records.

            PAGE STRUCTURE

            To transmit files that are discontinuous, FTP defines a page
            structure.  Files of this type are sometimes known as
            "random access files" or even as "holey files".  In these
            files there is sometimes other information associated with
            the file as a whole (e.g., a file descriptor), or with a
            section of the file (e.g., page access controls), or both.
            In FTP, the sections of the file are called pages.

            To provide for various page sizes and associated
            information, each page is sent with a page header.  The page
            header has the following defined fields:

               Header Length

                  The number of logical bytes in the page header
                  including this byte.  The minimum header length is 4.

               Page Index

                  The logical page number of this section of the file.
                  This is not the transmission sequence number of this
                  page, but the index used to identify this page of the
                  file.

               Data Length

                  The number of logical bytes in the page data.  The
                  minimum data length is 0.

               Page Type

                  The type of page this is.  The following page types
                  are defined:

                     0 = Last Page

                        This is used to indicate the end of a paged
                        structured transmission.  The header length must
                        be 4, and the data length must be 0.

                     1 = Simple Page

                        This is the normal type for simple paged files
                        with no page level associated control
                        information.  The header length must be 4.

                     2 = Descriptor Page

                        This type is used to transmit the descriptive
                        information for the file as a whole.

                     3 = Access Controlled Page

                        This type includes an additional header field
                        for paged files with page level access control
                        information.  The header length must be 5.

               Optional Fields

                  Further header fields may be used to supply per page
                  control information, for example, per page access
                  control.

            All fields are one logical byte in length.  The logical byte
            size is specified by the TYPE command.  See Appendix I for
            further details and a specific case at the page structure.

      A note of caution about parameters:  a file must be stored and
      retrieved with the same parameters if the retrieved version is to
      be identical to the version originally transmitted.  Conversely,
      FTP implementations must return a file identical to the original
      if the parameters used to store and retrieve a file are the same.

      TRANSMISSION MODES

      The next consideration in transferring data is choosing the
      appropriate transmission mode.  There are three modes: one which
      formats the data and allows for restart procedures; one which also
      compresses the data for efficient transfer; and one which passes
      the data with little or no processing.  In this last case the mode
      interacts with the structure attribute to determine the type of
      processing.  In the compressed mode, the representation type
      determines the filler byte.

      All data transfers must be completed with an end-of-file (EOF)
      which may be explicitly stated or implied by the closing of the
      data connection.  For files with record structure, all the
      end-of-record markers (EOR) are explicit, including the final one.
      For files transmitted in page structure a "last-page" page type is
      used.

      NOTE:  In the rest of this section, byte means "transfer byte"
      except where explicitly stated otherwise.

      For the purpose of standardized transfer, the sending host will
      translate its internal end of line or end of record denotation
      into the representation prescribed by the transfer mode and file
      structure, and the receiving host will perform the inverse
      translation to its internal denotation.  An IBM Mainframe record
      count field may not be recognized at another host, so the
      end-of-record information may be transferred as a two byte control
      code in Stream mode or as a flagged bit in a Block or Compressed
      mode descriptor.  End-of-line in an ASCII or EBCDIC file with no
      record structure should be indicated by <CRLF> or <NL>,
      respectively.  Since these transformations imply extra work for
      some systems, identical systems transferring non-record structured
      text files might wish to use a binary representation and stream
      mode for the transfer.

      The following transmission modes are defined in FTP:

         STREAM MODE

         The data is transmitted as a stream of bytes.  There is no
         restriction on the representation type used; record structures
         are allowed.

         In a record structured file EOR and EOF will each be indicated
         by a two-byte control code.  The first byte of the control code
         will be all ones, the escape character.  The second byte will
         have the low order bit on and zeros elsewhere for EOR and the
         second low order bit on for EOF; that is, the byte will have
         value 1 for EOR and value 2 for EOF.  EOR and EOF may be
         indicated together on the last byte transmitted by turning both
         low order bits on (i.e., the value 3).  If a byte of all ones
         was intended to be sent as data, it should be repeated in the
         second byte of the control code.

         If the structure is a file structure, the EOF is indicated by
         the sending host closing the data connection and all bytes are
         data bytes.

         BLOCK MODE

         The file is transmitted as a series of data blocks preceded by
         one or more header bytes.  The header bytes contain a count
         field, and descriptor code.  The count field indicates the
         total length of the data block in bytes, thus marking the
         beginning of the next data block (there are no filler bits).
         The descriptor code defines:  last block in the file (EOF) last
         block in the record (EOR), restart marker (see the Section on
         Error Recovery and Restart) or suspect data (i.e., the data
         being transferred is suspected of errors and is not reliable).
         This last code is NOT intended for error control within FTP.
         It is motivated by the desire of sites exchanging certain types
         of data (e.g., seismic or weather data) to send and receive all
         the data despite local errors (such as "magnetic tape read
         errors"), but to indicate in the transmission that certain
         portions are suspect).  Record structures are allowed in this
         mode, and any representation type may be used.

         The header consists of the three bytes.  Of the 24 bits of
         header information, the 16 low order bits shall represent byte
         count, and the 8 high order bits shall represent descriptor
         codes as shown below.

         Block Header

            +----------------+----------------+----------------+
            | Descriptor     |    Byte Count                   |
            |         8 bits |                      16 bits    |
            +----------------+----------------+----------------+


         The descriptor codes are indicated by bit flags in the
         descriptor byte.  Four codes have been assigned, where each
         code number is the decimal value of the corresponding bit in
         the byte.

            Code     Meaning

             128     End of data block is EOR
              64     End of data block is EOF
              32     Suspected errors in data block
              16     Data block is a restart marker

         With this encoding, more than one descriptor coded condition
         may exist for a particular block.  As many bits as necessary
         may be flagged.

         The restart marker is embedded in the data stream as an
         integral number of 8-bit bytes representing printable
         characters in the language being used over the control
         connection (e.g., default--NVT-ASCII).  <SP> (Space, in the
         appropriate language) must not be used WITHIN a restart marker.

         For example, to transmit a six-character marker, the following
         would be sent:

            +--------+--------+--------+
            |Descrptr|  Byte count     |
            |code= 16|             = 6 |
            +--------+--------+--------+

            +--------+--------+--------+
            | Marker | Marker | Marker |
            | 8 bits | 8 bits | 8 bits |
            +--------+--------+--------+

            +--------+--------+--------+
            | Marker | Marker | Marker |
            | 8 bits | 8 bits | 8 bits |
            +--------+--------+--------+

         COMPRESSED MODE

         There are three kinds of information to be sent:  regular data,
         sent in a byte string; compressed data, consisting of
         replications or filler; and control information, sent in a
         two-byte escape sequence.  If n>0 bytes (up to 127) of regular
         data are sent, these n bytes are preceded by a byte with the
         left-most bit set to 0 and the right-most 7 bits containing the
         number n.

         Byte string:

             1       7                8                     8
            +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+     +-+-+-+-+-+-+-+-+
            |0|       n     | |    d(1)       | ... |      d(n)     |
            +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+     +-+-+-+-+-+-+-+-+
                                          ^             ^
                                          |---n bytes---|
                                              of data

            String of n data bytes d(1),..., d(n)
            Count n must be positive.

         To compress a string of n replications of the data byte d, the
         following 2 bytes are sent:

         Replicated Byte:

              2       6               8
            +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
            |1 0|     n     | |       d       |
            +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

         A string of n filler bytes can be compressed into a single
         byte, where the filler byte varies with the representation
         type.  If the type is ASCII or EBCDIC the filler byte is <SP>
         (Space, ASCII code 32, EBCDIC code 64).  If the type is Image
         or Local byte the filler is a zero byte.

         Filler String:

              2       6
            +-+-+-+-+-+-+-+-+
            |1 1|     n     |
            +-+-+-+-+-+-+-+-+

         The escape sequence is a double byte, the first of which is the
         escape byte (all zeros) and the second of which contains
         descriptor codes as defined in Block mode.  The descriptor
         codes have the same meaning as in Block mode and apply to the
         succeeding string of bytes.

         Compressed mode is useful for obtaining increased bandwidth on
         very large network transmissions at a little extra CPU cost.
         It can be most effectively used to reduce the size of printer
         files such as those generated by RJE hosts.

# source: https://tools.ietf.org/html/rfc959
"""

TYPE1_ASCII    ='A'
TYPE1_IMAGE    ='I'
TYPE1_EBCDIC   ='E'
TYPE1_LOCALBYTE='L'
TYPE2_NONPRINT ='N'
TYPE2_TELNET   ='T'
TYPE2_CARRIAGE ='C'

SEEK_SET = 0
SEEK_CUR = 1
SEEK_END = 2

def emit_hash(tag, fp):
  fp.seek(0,SEEK_SET)
  print(tag, hashlib.sha256(fp.getvalue()).hexdigest())

def emit_rank(tag,fp):
  fp.seek(0,SEEK_END)
  print(tag, fp.tell())

def emit_hex(tag,fp):
  fp.seek(0,SEEK_SET)
  print ( ''.join( [ "%02X " % ord( x ) for x in fp.getvalue() ] ).strip())


def test1(SERVER, USER, PASS, kat):
 indexTime = "[" + strftime("%m/%d/%Y %H:%M:%S %p %Z",localtime()) + "]"
 try:
  ftp = ftplib_indeterminant.FTP(SERVER,timeout=3)     # connect to host, default port
  ftp.login(USER,PASS)
  ftp.set_pasv(True)

  print ("test1")
  print ("STOR /TEST1i")
  kat.seek(0,SEEK_SET)
  ftp.storindeterminate('STOR /TEST1i', kat)
  print ("STOR /TEST1b")
  kat.seek(0,SEEK_SET)
  ftp.storbinary('STOR /TEST1b', kat)
  print ("STOR /TEST1l")
  kat.seek(0,SEEK_SET)
  ftp.storlines('STOR /TEST1l', kat)

  test1ii = BytesIO()
  test1ib = BytesIO()
  test1il = BytesIO()
  test1bi = BytesIO()
  test1bb = BytesIO()
  test1bl = BytesIO()
  test1li = BytesIO()
  test1lb = BytesIO()
  test1ll = BytesIO()

  print ("RETR /TEST1xy")
  ftp.retrindeterminate('RETR /TEST1i', test1ii.write)
  ftp.retrindeterminate('RETR /TEST1b', test1bi.write)
  ftp.retrindeterminate('RETR /TEST1l', test1li.write)
  ftp.retrbinary('RETR /TEST1i', test1ib.write)
  ftp.retrbinary('RETR /TEST1b', test1bb.write)
  ftp.retrbinary('RETR /TEST1l', test1lb.write)
  ftp.retrlines('RETR /TEST1i', test1il.write)
  ftp.retrlines('RETR /TEST1b', test1bl.write)
  ftp.retrlines('RETR /TEST1l', test1ll.write)

  print("sha256 hash")
  emit_hash("cntrl: ",kat)
  emit_hash("ii:    ",test1ii)
  emit_hash("ib:    ",test1ib)
  emit_hash("il:    ",test1il)
  emit_hash("bi:    ",test1bi)
  emit_hash("bb:    ",test1bb)
  emit_hash("bl:    ",test1bl)
  emit_hash("li:    ",test1li)
  emit_hash("lb:    ",test1lb)
  emit_hash("ll:    ",test1ll)

  print("rank")
  emit_rank("cntrl: ",kat)
  emit_rank("ii:    ",test1ii)
  emit_rank("ib:    ",test1ib)
  emit_rank("il:    ",test1il)
  emit_rank("bi:    ",test1bi)
  emit_rank("bb:    ",test1bb)
  emit_rank("bl:    ",test1bl)
  emit_rank("li:    ",test1li)
  emit_rank("lb:    ",test1lb)
  emit_rank("ll:    ",test1ll)

  #emit_hex("cntrl: ",kat)
  #emit_hex("ii:    ",test1ii)
  #emit_hex("ib:    ",test1ib)
  #emit_hex("il:    ",test1il)
  #emit_hex("bi:    ",test1bi)
  #emit_hex("bb:    ",test1bb)
  #emit_hex("bl:    ",test1bl)
  #emit_hex("li:    ",test1li)
  #emit_hex("lb:    ",test1lb)
  #emit_hex("ll:    ",test1ll)
  print ("/test1")
  endTime = "[" + strftime("%m/%d/%Y %H:%M:%S %p %Z",localtime()) + "]"
  print "%s, %s, %s, %s, 0, 0" % (startTime, endTime, SERVER, USER)
except (ftplib_indeterminant.error_temp,ftplib_indeterminant.error_reply,ftplib_indeterminant.error_perm,ftplib_indeterminant.error_proto) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e.args[0] )
except (socket.error) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e) 
except (IOError) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e.strerror )
finally:
 pass
  ftp.quit()

def test2(SERVER, USER, PASS, kat):
 startTime = "[" + strftime("%m/%d/%Y %H:%M:%S %p %Z",localtime()) + "]"
 try:
  ftp = ftplib_indeterminant.FTP(SERVER,timeout=3)     # connect to host, default port
  ftp.login(USER,PASS)
  ftp.set_pasv(False)
  print ("test2")
  ftp.set_pasv(False)
  test2ii = BytesIO()
  test2ib = BytesIO()
  test2il = BytesIO()
  test2bi = BytesIO()
  test2bb = BytesIO()
  test2bl = BytesIO()
  test2li = BytesIO()
  test2lb = BytesIO()
  test2ll = BytesIO()

  print ("STOR /TEST2")
  kat.seek(0,SEEK_SET)
  ftp.storindeterminate('STOR /TEST2i', kat)
  kat.seek(0,SEEK_SET)
  ftp.storbinary('STOR /TEST2b', kat)
  kat.seek(0,SEEK_SET)
  ftp.storlines('STOR /TEST2l', kat)

  print ("RETR /TEST2")
  ftp.retrindeterminate('RETR /TEST2i', test2ii.write)
  ftp.retrindeterminate('RETR /TEST2b', test2bi.write)
  ftp.retrindeterminate('RETR /TEST2l', test2li.write)

  ftp.retrbinary('RETR /TEST2i', test2ib.write)
  ftp.retrbinary('RETR /TEST2b', test2bb.write)
  ftp.retrbinary('RETR /TEST2l', test2lb.write)

  ftp.retrlines('RETR /TEST2i', test2il.write)
  ftp.retrlines('RETR /TEST2b', test2bl.write)
  ftp.retrlines('RETR /TEST2l', test2ll.write)

  print("sha256 hash")
  emit_hash("cntrl: ",kat)
  emit_hash("ii:    ",test2ii)
  emit_hash("ib:    ",test2ib)
  emit_hash("il:    ",test2il)
  emit_hash("bi:    ",test2bi)
  emit_hash("bb:    ",test2bb)
  emit_hash("bl:    ",test2bl)
  emit_hash("li:    ",test2li)
  emit_hash("lb:    ",test2lb)
  emit_hash("ll:    ",test2ll)

  print("rank")
  emit_rank("cntrl: ",kat)
  emit_rank("ii:    ",test2ii)
  emit_rank("ib:    ",test2ib)
  emit_rank("il:    ",test2il)
  emit_rank("bi:    ",test2bi)
  emit_rank("bb:    ",test2bb)
  emit_rank("bl:    ",test2bl)
  emit_rank("li:    ",test2li)
  emit_rank("lb:    ",test2lb)
  emit_rank("ll:    ",test2ll)

  #emit_hex("cntrl: ",kat)
  #emit_hex("ii:    ",test2ii)
  #emit_hex("ib:    ",test2ib)
  #emit_hex("il:    ",test2il)
  #emit_hex("bi:    ",test2bi)
  #emit_hex("bb:    ",test2bb)
  #emit_hex("bl:    ",test2bl)
  #emit_hex("li:    ",test2li)
  #emit_hex("lb:    ",test2lb)
  #emit_hex("ll:    ",test2ll)
  print ("/test2")
  endTime = "[" + strftime("%m/%d/%Y %H:%M:%S %p %Z",localtime()) + "]"
  print "%s, %s, %s, %s, 0, 0" % (startTime, endTime, SERVER, USER)
except (ftplib_indeterminant.error_temp,ftplib_indeterminant.error_reply,ftplib_indeterminant.error_perm,ftplib_indeterminant.error_proto) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e.args[0] )
except (socket.error) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e) 
except (IOError) as e:
 print "%s, %s, %s, %s, 1, %s" % (startTime, endTime, SERVER, USER, e.strerror )
finally:
 pass
  ftp.quit()

kat     = BytesIO(b"\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF6\xF7\xF8\xF9\xFA\xFB\xFC\xFD\xFE\xFF")
kat.seek(0,SEEK_END)
kat.write(BytesIO(b"\xC0\xC1\xC2\xC3\xC4\xC5\xC6\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD6\xD7\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF").getvalue())
kat.write(BytesIO(b"\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xA9\xAA\xAB\xAC\xAD\xAE\xAF\xB0\xB1\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xBA\xBB\xBC\xBD\xBE\xBF").getvalue())
kat.write(BytesIO(b"\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F").getvalue())
kat.write(BytesIO(b"\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A\x7B\x7C\x7D\x7E\x7F").getvalue())
kat.write(BytesIO(b"\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5A\x5B\x5C\x5D\x5E\x5F").getvalue())
kat.write(BytesIO(b"\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F").getvalue())
kat.write(BytesIO(b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F").getvalue())
kat.seek(0,SEEK_SET)
view = kat.getvalue()
kat.seek(0,SEEK_END)
kat.write(view)
kat.seek(0,SEEK_SET)
view = kat.getvalue()
kat.seek(0,SEEK_END)
kat.write(view)
kat.seek(0,SEEK_SET)

#TODO outboard {S,U,P} tuple.

  SERVER = ''
  USER   = ''
  PASS   = ''
  test1(SERVER, USER, PASS, kat)
  test2(SERVER, USER, PASS, kat)

